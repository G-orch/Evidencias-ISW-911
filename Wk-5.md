# Semana 5 KPI en Power BI

## Como definir un indicador de desempeño

- S
- M
- A
- R
- T
  
Cuando se crean dashboard o tableros no se debe de poblar el tablero, y se deben de colocar de izquierda a derecha, de arriba hacia abajo en orden de tamaño; además los filtros a utilizar en la parte superior. No se deben de poner muchas visualizaciónes por que se pierde el enfoque.

## Análisis exploratorio de datos

- EDA
  
- Etapas o fases del análisis exploratorio de datos:

Metodología utilizada para analizar conjuntos de datos y revelar patrones.

- Exploración inicial
- Manejo de datos faltantes
- Análisis univariados
- Análisis bivariado
- Visualizacióm de datos
- Análisis multivariado
- Limpieza de datos
- Idenitificación y manejo de valores faltantes
- Tratamiento de valores atípicos
- Eliminación de duplicados
- Normalización y estandarización de datos
- Revelar patrones y tendencias
- Preparar datos para modelos avanzados
- Validar suposiciones
- Visualización clara de los datos

El análisis descriptivo es esencial en cualquier tupo de investigacióon o análisis de datos.

métdos comunes:

- Medidas de tendencia dentral
- Medidas de dispersión
- Gráficos estadísticos
- Medidas de asimetría u kurtosis
- Tablas de contingencia y gráficos de barras agrupadas


## Herramienta Colab

Instalar R y R studio


![Imagen]()

---

- [Volver a la tabla de contenidos](readme.md)