# Tabla de contenidos actualizada

- [Semana 1](Wk-1.md)............... Conceptos iniciales
- [Semana 2](Wk-2.md)............... Nuevas Funciones
- [Semana 3](Wk-3.md)............... Técnicas para transferencia de datos
- [Semana 4](Wk-4.md)............... Creación y población de tablas del data warehouse
- [Semana 5](Wk-5.md)............... Semana 5 KPI en Power BI
- [Semana 6](Wk-6.md)............... R y R Studio
- [Semana 7](Wk-7.md)............... Apache Spark
- [Semana 8](Wk-8.md)............... Minería web - text minning
- [Semana 9](Wk-9.md)............... Presentación proyecto I
- [Semana 10](Wk-10.md)............. Semana santa
- [Semana 11](Wk-11.md)............. Regresión lineal, simple y múltiple.
- [Semana 12](Wk-12.md)............. Modelos predictivos- Ejemplos
- [Semana 13](Wk-13.md)............. Feriado
- [Semana 14](Wk-14.md)............. Presentación proyecto II y III

