# Semana 11 Regresión lineal, simple y múltiple.

## Regresión Múltiple

```R
#librerias   
install.packages("GGally")
install.packages("caret")

library(GGally)
library(corrplot)
library(PerformanceAnalytics)
require(psych)
library(dplyr)
library(caret)

datos<-read.csv("D:/UTN/2024/l Cuatrimestre/Mineria de datos/Semana 10/rentadebicis.csv",fileEncoding = "latin1", header = T,sep = ",")
summary(datos)
plot(datos$rentas_totales)
plot(datos$rentas_totales, type = "b")
hist(datos$rentas_totales)


plot(datos$rentas_totales, type = "b", pch = 19, 
     col = "green", xlab = "x", ylab = "y")

View(table( cor(datos)))

corrplot(cor (datos),method = "circle")

names(datos)
variables<-data.frame(datos$temperatura,datos$humedad,datos$sensacion_termica)
chart.Correlation(variables, histogram = TRUE, method = "pearson")


plot (datos$temperatura,datos$rentas_totales, 
      main = " influencia de la temperaturasobre las ventas",
      xlab = "temperatura",ylab = "cantidad de rentas")

c1 <- rainbow(10)
c2 <- rainbow(10, alpha=0.2)
c3 <- rainbow(10, v=0.7)


par(mfrow=c(3,5))
for (i in 1:14) {
  boxplot(datos[,i],main=names(datos)[i],col=c2, medcol=c3, whiskcol=c1, 
          staplecol=c3, boxcol=c3, outcol=c3, pch=23, cex=1)
}

# PARTICIONAMIENTO  DE LOS DATOS
t.id<-createDataPartition(datos$rentas_totales,p=0.7,list = F)
View(t.id)

# crear el modelo de regresion multiple
regresion <- lm(data = datos[t.id,], rentas_totales ~ 
                  hora+dia+mes+año+estacion+dia_de_la_semana+asueto+
                  temperatura+sensacion_termica+humedad+velocidad_del_viento)

regresion <- lm(data = datos[t.id,-c(12,13)], rentas_totales ~. )

step(regresion)

#Intervalos de confianza para los coeficientes del modelo
confint(regresion)

#evaluar el modelo para ver cuales son las mejores variables 
#para recalibrar el modelo
summary(regresion)

step(regresion, direction = "both", trace = 1)

# se selecionar las variables y se reprocesa el modelo
regresion <- lm(data = datos[t.id,], rentas_totales ~hora+mes+año)

# el modelo no es tan exacto con los residios se pueden distribuir los residuos
library(dplyr)
boxplot (regresion$residuals)

#Prediccion con los datos de testeo 
#
prediccion<-predict(regresion,datos[-t.id,-c(12,13)])

# Agregar predicciones al conjunto de datos de prueba
datos_predichos <- datos[-t.id,-c(12,13)]  # Copiar el conjunto de datos de prueba
datos_predichos$prediccion <- predict(regresion, newdata = datos[-t.id,-c(12,13)])  # Agregar la columna de predicciones

# Agregar predicciones redondeadas al conjunto de datos de prueba
datos_predichos$prediccion_redondeada <- round(datos_predichos$prediccion)

# Visualizar el conjunto de datos con las predicciones
head(datos_predichos)
View(datos_predichos)

# contruir el modelo de preduccion con datos nuevos

datos_nuevos<-data.frame(hora=1,mes=2,año=2023)
predict(regresion,datos_nuevos)


#El error cuadrático medio (MSE por sus siglas en inglés, Mean Squared Error) 
#es una medida que nos ayuda a entender cuán cerca están las predicciones de un 
#modelo estadístico de los valores reales. Para calcularlo, tomamos la diferencia entre 
#cada valor predicho y su valor real, lo elevamos al cuadrado para que todos los valores
#sean positivos y para dar más peso a los errores grandes, luego promediamos estos valores

#Error Cuadrático Medio (MSE): Mide la media de los errores cuadrados entre las predicciones y 
#los valores reales. Cuanto más bajo sea el MSE, mejor será el rendimiento del modelo.


mse <- mean((regresion$residuals)^2)

sqrt(mean((regresion$fitted.values-datos[t.id,-c(12,13)]$rentas_totales)^2))

# Calcular el Error Cuadrático Medio (ECM)
mse <- mean((datos_predichos$rentas_totales - datos_predichos$prediccion_redondeada)^2)

# Imprimir el Error Cuadrático Medio (ECM)
cat("Error Cuadrático Medio (MSE):", mse, "\n")

```

## Regresión Simple

```R
library(car)
library(ggplot2)
library(xlsx)
library(dplyr)


install.packages("boot")
install.packages("QuantPsyc")

datos<-read.xlsx("D:/UTN/2024/l Cuatrimestre/Mineria de datos/Semana 10/sales.xlsx",sheetIndex = 1)
names(datos)<-c("publicidad","cantidad_ventas")
View(datos)
names(datos)
attach(datos)

glimpse(datos)

# revisar valores atipicos
boxplot(datos$publicidad)
boxplot(datos$cantidad_ventas)
# ver si hay nulos en el conjunto de datos
which( is.na(datos))
# ver  cuales son los valores atipicos
datos %>% filter(publicidad<=2000) %>% 
  summarise(mediana=median(publicidad))
# ver la concentracion de los datos 
hist(datos$publicidad,freq = F, col = "green")
lines(density(datos$publicidad),col="red",lty = 2, lwd = 3)

datos$publicidadlog<-log(datos$publicidad)
hist(datos$publicidadlog,freq = F, col = "green")
lines(density(datos$publicidadlog),col="red",lty = 2, lwd = 3)

# tratar los valores atipicos

outlayers <- function(data, inferior, superior) {
  data[data < inferior] <- mean(data)
  data[data > superior] <- median(data)
  data
}

# agregarmos la nueva columna con los datos tratados
cdatos<-datos %>% mutate(cpublicidad= outlayers( datos$publicidad,10,1900))
boxplot(cdatos$cpublicidad)

modelo=lm(data = datos,cantidad_ventas ~ publicidad)

summary(modelo)
# p-value tiene que ser mayor que 0.05  para ser valido

# Multiple R-squared:  0.3346,
# lo anterior nos da que tanto explica las ventas la publicidad 
# lo ideal es que Multiple R-squared  este mas cercano a 1 pero el 0.2784 de es muy bajo.

# entonces explica que :

#Ventas = intercepto + Beta1 + error
#ventas = 1.34 + 9.61 * publicidad

#quiere decir que por cada ($1.34) las ventas se incrementa en 9.61

#la correlacion de pearson 
#
cor.test(cdatos$cpublicidad,cdatos$cantidad_ventas)
#  A medida que aumenta la publicidad aunmentan las ventas
sqrt(0.2784)


plot(cdatos$cpublicidad,cdatos$cantidad_ventas,xlab = "Publicidad", 
     ylab = "Ventas",main = "Prueba de regresion lineal")

abline(modelo,col="blue")

# Graficamos mas  bonito 
ggplot(cdatos,aes(cpublicidad,cantidad_ventas))+
  geom_point(colour="red")+
  geom_smooth(method = "lm",colour="blue")

#datos_nuevos<-data.frame(publicidad=5000,level=0.95,interval="prediction")
#
predict(modelo,data.frame(cpublicidad=1000))
boxplot(cdatos$cpublicidad)

resultado<-data.frame(cdatos$cpublicidad,
                      cdatos$cantidad_ventas,
                      modelo$fitted.values,
                      round(cdatos$cantidad_ventas-modelo$fitted.values)
)

names(resultado)<-c("publicidad","cantidad_ventas","Predicho" ,"diferencia")

View(resultado)



# R-squared y R-squared ajustado
cat("R-squared:", summary(modelo)$r.squared, "\n")
cat("Adjusted R-squared:", summary(modelo)$adj.r.squared, "\n")

# Error Cuadrático Medio (MSE)
mse <- mean((modelo$residuals)^2)
cat("Mean Squared Error (MSE):", mse, "\n")

# Gráficos de Residuos
par(mfrow=c(2,2))
plot(modelo)
```


## Regresión Logística

```R
# 
datos<-read.csv("D:/UTN/2024/l Cuatrimestre/Mineria de datos/Semana 10/titanic.csv",header = T,sep = ",")
attach(datos)
# realizamos el EDA
View(datos)
datos<- na.omit(datos)
names(datos)
library(dplyr)
library(caret)
glimpse(datos)
head(datos,2)
datos_modelo<-datos %>%  select(Pclass,Sex,Age,Survived)
datos_modelo$Survived <- factor(datos_modelo$Survived, levels = c(0,1))

attach(datos_modelo)
names(datos_modelo)
head(datos_modelo)

c2<-"red"
c1<-"blue"
c3<-"green"

boxplot(Pclass,main="Clase del pasajero",
        col=c2, medcol=c3, whiskcol=c1, staplecol=c3, 
        boxcol=c3, outcol=c3, pch=23, cex=1)

boxplot(Age,main="Edad del pasajero", pch=23, cex=2,col="blue")

which(is.na(datos_modelo))
mean(is.na(datos_modelo)) 
sum(is.na(datos_modelo$Age))
dim(datos_modelo)
summary(datos_modelo)

#podemos eliminar columnas con valores nulos si lo decidimos
#datos_modelo<-na.omit(datos_modelo)

datos_modelo<-na.omit(datos_modelo)
which(is.na(datos_modelo))

casos_edad <-datos_modelo %>% mutate(grupo=ifelse(Age >54,1,2)) %>% 
  group_by(grupo) %>% 
  summarise(total=n())
casos_edad$grupo<-as.factor(casos_edad$grupo)

casos_edad <-casos_edad %>% mutate(porcentage = total / sum(total)*100)

#stat = "identity" es para graficar  una variable CHAR con una numerica
#position = "dodge"  es por si esta aplicado

ggplot(casos_edad,aes(x=grupo,y=porcentage))+
  geom_bar(stat = "identity",fill = c("dodgerblue4","firebrick4"))+
  ggtitle ("Grafico de grupo de edades")+
  theme (plot.title = element_text(size=rel(1), #Tamaño relativo de la letra del título
                                   vjust=2, #Justificación vertical, para separarlo del gráfico
                                   face="bold", #Letra negrilla. Otras posibilidades "plain", "italic", "bold" y "bold.italic"
                                   color="red", #Color del texto
                                   lineheight=1.5)) + #Separación entre líneas
  geom_text(aes(label=paste0(round(porcentage,2),"% - ",total)),size=6, color="blue",vjust=-0.3) +
  labs(x = "Grupos de Edad",y = "Porcentaje") + # Etiquetas o títulos de los ejes
  #theme(axis.title = element_text(face="italic", colour="brown", size=rel(1.5))) # Tamaño de los títulos de los ejes
  theme(axis.title.x = element_text(face="bold", vjust=-0.5, colour="orange", size=rel(1))) +
  theme(axis.title.y = element_text(face="bold", vjust=1.5, colour="blue", size=rel(1))) 



ggplot(casos_edad,aes(x=grupo,y=total,fill=grupo)) + 
  geom_bar(stat = "identity", position = "dodge")+
  geom_text(aes(label=total),size=10, color="blue")



# hacemos la particion de los datos 
set.seed(2023)
View(datos_modelo)

#Tipo de Variables (Dependiente y Independientes) 
datos_modelo<-datos %>%  select(Pclass,Sex,Age,Survived)
datos_modelo$Survived <- factor(datos_modelo$Survived, levels = c(0,1))

train <- createDataPartition(datos_modelo$Survived, p=0.7, list = F)

glm_mod<-glm( formula = Survived ~ .,data = datos_modelo[train,] , family = "binomial")

summary(glm_mod)

# generamos las probabilidades del modelos
probabilidad <-predict(glm_mod,type = "response")

#creamos la curva ROC
#el area bajo la curva AUC debe  ser mayor a 0.5 para decir que el modelo se puede usar
#ver el resultdo del modelo.. en % de efectividad
#el punto donde decide 1  o 0 se debe revisar para hacer la clasificacion

library(pROC)
curva <-roc(datos_modelo[train,"Survived"],
            probabilidad,auc = T,
            ci=T,
            levels = c(0, 1)
            #,   direction = "<"
)

print(curva)
##windows(height = 10,width = 20)

plot.roc(main="Curva ROC del modelo logistico",
         curva,legacy.axes = T,
         print.thres = "best",
         print.auc = T,
         of="thresholds",
         thresholds=T,
         grid = T,
         type = "shape",
         col="#1c61b6AA"
)


# realizarmos las predicciones sobre el conjunto de datos totales
valor_optimo<-0.460

prediccion<-predict(object= glm_mod,newdata = datos_modelo[train,] ,type = 'response') 
prediccion<-ifelse(prediccion >valor_optimo,yes = 1,no = 0)

#Comparar los resultados
#
datos_modelo[train,] %>% mutate(predicho=prediccion) %>% 
  select(predicho,everything()) %>% 
  mutate(comp=predicho==Survived)-> resultado

View(resultado)

# vemos la efectividad del modelo  basicamente extrayecto la media de los  resultados
mean(resultado$comp) # 81%
# otra forma de ver la exactitud del modelos
install.packages("MLmetrics")
library(MLmetrics)
Accuracy(y_pred = prediccion,y_true = datos_modelo[train,"Survived"] )

# comparar los datos 
#Generamos la informacion para los datos de entrenamiento

datos_modelo[train,"prob_exito"] <- predict(glm_mod, newdata = datos_modelo[train,], type="response")
datos_modelo[train,"pred_optima"] <- ifelse(datos_modelo[train, "prob_exito"]>=valor_optimo, 1, 0)

table(datos_modelo[train,"Survived"], 
      datos_modelo[train,"pred_optima"], 
      dnn=c("Actual","Predicho"))

# otra forma de ver la matrix de confusion 
ConfusionMatrix(y_pred = prediccion,y_true = datos_modelo[train,"Survived"])
# el area bajo la curva
AUC(y_pred = prediccion,y_true = datos_modelo[train,"Survived"])
# el error
#MSE(y_pred = prediccion,y_true = Survived)

# PROBANDO EL MODELO CON TEST

# ejecutamos el modelo para el conjunto de datos de prueba
prediccion_test<-predict(object= glm_mod,newdata = datos_modelo[-train,] ,type = 'response') 
prediccion_test<-ifelse(prediccion_test >valor_optimo,yes = 1,no = 0)

Accuracy(y_pred = prediccion_test,y_true = datos_modelo[-train,"Survived"] )

datos_modelo[-train,"prob_exito"]<- predict(glm_mod, newdata = datos_modelo[-train,], type="response")
datos_modelo[-train,"pred_optima"] <- ifelse(datos_modelo[-train,"prob_exito"]>=0.430, 1, 0)

# imprimimos la matriz de  confucion
table(datos_modelo[-train,"Survived"], 
      datos_modelo[-train,"pred_optima"], 
      dnn=c("Actual","Predicho"))

View(datos_modelo)


## Para evaluar un modelo usamos el error cuadratico medio
```

## Cluster Gerárquico

```R
library(dplyr)

protein <- read.csv("D:/UTN/2024/l Cuatrimestre/Mineria de datos/Semana 10/protein.csv",sep = ";")
head(protein)
View(protein)
data <- as.data.frame(scale(protein[,-1]))
View(data)
data$Country = protein$Country
rownames(data) = data$Country
names(data)
View(data)
data$Country<-NULL
hc <- hclust(dist(data, method = "euclidean"),
             method = "ward.D2")
hc
hcd <- as.dendrogram(hc)

nodePar <- list(lab.cex = 0.6, pch = c(NA, 19), 
                cex = 0.7, col = "blue")

# Customized plot; remove labels
plot(hcd, ylab = "Height", nodePar = nodePar, leaflab = "none")
plot(hcd,  xlab = "Height",
     nodePar = nodePar, horiz = TRUE)

plot(hcd, xlim = c(1, 20), ylim = c(1,8))

plot(hcd,  xlab = "Height", nodePar = nodePar, 
     edgePar = list(col = 2:3, lwd = 2:1))


fit <- cutree(hc, k=4)
table(fit)
rect.hclust(hc, k=4, border="steelblue")

# install.packages("ape")
library("ape")
library("Rcpp")
library("ape")
library("Rcpp")

par(mar = c(5, 5, 2, 2), cex.axis = 0.5, cex.lab = 0.7, pin = c(6, 6), mai = c(0.8, 0.8, 0.2, 0.2))
plot(as.phylo(hc), type = "unrooted", cex = 0.8, no.margin = TRUE, lwd = 2, font = 3)

colors = c("red", "blue", "green", "black")
clus4 = cutree(hc, 4)
plot(as.phylo(hc), type = "fan",  font = 4, tip.color = colors[clus4],
     label.offset = 1, cex = 0.7,lwd = 2)

```
## Kmeans

```R

aseguradora <- paste(getwd(),"D:/UTN/2024/l Cuatrimestre/Mineria de datos/Semana 10/insurance.csv", sep = "")

insurance <- read.csv("D:/UTN/2024/l Cuatrimestre/Mineria de datos/Semana 10/insurance.csv",  encoding="UTF-8", header=TRUE, sep=",", 
                      na.strings="NA", dec=".", strip.white=TRUE)
View(insurance)


insurance.scale <- as.data.frame(scale(insurance[,5:9])) # escalar los datos
set.seed(80) # fijar semilla
insurance.km <- kmeans(insurance.scale, centers = 4) # Realizamos clustering

names(insurance.km) # contenido del objeto
head(insurance.km$cluster)
insurance.km$totss # inercia total
insurance.km$betweenss # inercia ínter grupos lo mas alta posible
insurance.km$withinss # inercia intra grupos 
insurance.km$tot.withinss # inercia intra grupos (total)  lo menor posible

sumbt<-kmeans(insurance.scale, centers = 1)$betweenss
# le agrego de 2  a 10 cluster para ver cuantos son adecuados
for(i in 2:10) sumbt[i] <- kmeans(insurance.scale, centers = i)$betweenss
plot(1:10, sumbt, type = "b", xlab = "número de clusters", 
     ylab = "suma de cuadrados ínter grupos")

# tambien se puede identificar aquel valor a partir del cual la reducción
#en la suma total de varianza intra-cluster deja de ser sustancial.
#install.packages("factoextra")
library(factoextra)
fviz_cluster(insurance.km, insurance.scale,main = "Grafico de Kmeans")

# con la inercia inter grupos se puede sacar la cantidad de clusters adecuados
#El cluster rojo representa la gente que es fiel y su experiendia
plot(insurance$ant_comp,insurance$ant_perm, col=insurance.km$cluster ,
     xlab = "Fidelidad a la compañía", ylab = "Experiencia" )

#por ejemplo para la mayor siniestralidad
aggregate(insurance[,5:9] ,by = list(insurance.km$cluster), mean)

```

## Árbol de regresión

```R
library(rpart.plot)
library(rpart)
library(caret)

data("airquality")
head(airquality)
View(airquality)

datos<-na.omit(airquality)
head(datos)

training.ids <- createDataPartition(datos$Ozone, p = 0.7, list = F)
modelo <-rpart(formula = Ozone~Solar.R+Wind+Temp , data = datos[training.ids,])

rpart.plot(modelo)
printcp(modelo)

modelo$cptable
printcp(modelo)
modelo_pruned <- prune(modelo, modelo$cptable[4, "CP"])
pruned<-prune(modelo_pruned,cp=0.020210 )
rpart.plot(modelo_pruned)



library(dplyr)
prediccion <-predict(modelo_pruned, newdata = datos[training.ids,])
prediccion
training<- datos[training.ids,] %>% mutate (resultado=prediccion)

```

## Bosques aleatorios

```R
library(rpart) 
library(ROCR)
library(caret)
library(randomForest)
#install.packages("DMwR")

## ejemplo 1
library(randomForest)
library(dplyr)
library(PerformanceAnalytics)
library(caret)
library(rpart)
library(pROC)

## el RF es muy bueno quien le da pelea es la red neuronal

data("iris")
datos<-iris
View(datos)
tamano<-nrow(datos)
training<-round(tamano*0.7)
indices<-sample(1:tamano,size = training)

datos_train<-datos[indices,]
datos_test<-datos[-indices,]
chart.Correlation(datos_train[,-5])

help("randomForest")

modelo<-randomForest(Species~.,data=datos_train, 
                     ntree=500)

modelo
modelo$importance
varImpPlot(modelo)
plot(modelo)
getTree(modelo,1)
# hacemos las predicciones
predicciones<-predict(modelo,datos_test[,-5])
#  dos  formas
#  Errores o certezas del modelo
mc<-table(predicciones,datos_test$Species)
mc<-with(datos_test,table(predicciones,Species))
confusionMatrix(predicciones,datos_test$Species)


# para ver la efectividad del modelos
# sumamos los valores de la diagonal de la matriz entre el total de datos de la matriz
100 * sum(diag(mc)/sum(mc))

resultado <-datos_test %>% mutate(clasificacion= 
                                    predict(modelo, 
                                            datos_test[,-5])  )
View(resultado)
attach(resultado)
resultado %>% filter(Species!=clasificacion)
plot(modelo)



#--XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

## ejemplo 2 
load(file = "files/churn.RData") 
names(churnTrain) 
head(churnTrain)
View(churnTrain)
train <- churnTrain[,-c(1,3)] 
test <- churnTest[,-c(1,3)] 
View(train)

modelo.RF <- randomForest(churn~voice_mail_plan+international_plan+total_eve_charge,  
                          data = train,  
                          ntree=500,  
                          mtry=5,  
                          importance=T) 
modelo.RF 
varImpPlot(modelo.RF)
plot(modelo.RF)

names(modelo.RF) 

#Observamos los valores predichos para las primeras observaciones, 
#junto con las proporciones (o probabilidades) asociadas las clases yes y no. 

head(modelo.RF$predicted) 
head(modelo.RF$votes) 
modelo.RF$importance[,c(3,4)] 
varImpPlot(x = modelo.RF,sort = T, n.var = 10)


# utilizando GGPLOT
imp<-varImpPlot(x = modelo.RF,sort = T, n.var = 10)
imp <- as.data.frame(imp)
imp$varnames <- rownames(imp) # row names to column
rownames(imp) <- NULL  

ggplot(imp, aes(x=reorder(varnames, MeanDecreaseAccuracy), 
                y=MeanDecreaseAccuracy)) + 
  geom_point(col="red",size=5) +
  labs(title = "Importancia de las variables")+
  ylab("MeanDecreaseAccuracy") +
  xlab("Variable") +
  coord_flip()+
  theme_minimal()+
  theme(panel.background = element_rect(fill = "steelblue")  )+
  scale_color_discrete(name="Variable Group") 

# genramos las probabilidades

probs <- predict(modelo.RF, test, type = "prob")
head(probs)
#me quedo con los exitos y los datos de test 
pred <- prediction(probs[,1], test$churn)
perf <- performance(pred, "tpr", "fpr")


plot(perf,colorize=TRUE,type="l",xlab="False Positive Percentage", ylab="True Positive Percentage",
     lwd=5,col="steelblue")
abline(a=0,b=1)

AUC       <- performance(pred,measure="auc")
AUCaltura <- AUC@y.values

cost.perf <- performance(pred, measure ="cost")
opt.cut   <- pred@cutoffs[[1]][which.min(cost.perf@y.values[[1]])]

x<-perf@x.values[[1]][which.min(cost.perf@y.values[[1]])]
y<-perf@y.values[[1]][which.min(cost.perf@y.values[[1]])]
points(x,y, pch=20, col="red")

cat("AUC:", AUCaltura[[1]]) 
cat("Punto de corte óptimo:",opt.cut)

# gregamos la prediccion al conjunto de datos originales 

probs <- predict(modelo.RF, test, type = "prob")
test$pred <- ifelse(probs[,1] > opt.cut, "yes", "no")
View(test)

```

## Arbol de clasificación

```R
set.seed(1111)
churn<-read.csv("D:/UTN/2024/l Cuatrimestre/Mineria de datos/Semana 10/churn.csv",header = T,sep = ";")
View(churn)

#Usaremos solo algunas columnas (features)
names(churn[,c(2:8)]) # nombres en ingles
names(churn)[1]<-"cancelacion"
View(churn)
ind <- sample(2, nrow(churn), replace=TRUE, 
              prob=c(0.7, 0.3)) #train (60%) y test (40%)

trainData <- churn[ind==1, ] #train
testData <- churn[ind==2, ] #test

ArbolRpart <- rpart(cancelacion ~ ., method="class", 
                    data=trainData) 

#Formula: variable dependiente (Cancelacion) depende todas las otras


print(ArbolRpart)   

rpart.plot(ArbolRpart,extra=8)  # extra=4:probabilidad de observaciones por clase

printcp(ArbolRpart) 
plotcp(ArbolRpart)              # evolución del error a medida que se incrementan los nodos

ArbolRpart$cptable
# Podado del árbol
pArbolRpart <- prune(ArbolRpart, ArbolRpart$cptable[4, "CP"])
pArbolRpart<- prune(ArbolRpart, 
                    cp= ArbolRpart$cptable[which.min(
                      ArbolRpart$cptable[,"xerror"]),"CP"])
pArbolRpart<- prune(ArbolRpart, cp=0.016771)


printcp(pArbolRpart)
plotcp(pArbolRpart)
rpart.plot(pArbolRpart,extra=4)  # extra=4:probabilidad de observaciones por clase
# Validamos la capacidad de predicción del árbol con el fichero de validación
testPredRpart <- predict(pArbolRpart, 
                         newdata = testData, 
                         type = "class")
# Visualizamos una matriz de confusión
table(testPredRpart, testData$cancelacion)

sum(testPredRpart == testData$cancelacion) / 
  length(testData$cancelacion)*100


library(pROC)
# Calcular las probabilidades predichas para los datos de prueba
testProbRpart <- predict(pArbolRpart, 
                         newdata = testData, type = "prob")
# Generar la curva ROC y calcular el AUC
rocRpart <- roc(testData$cancelacion, testProbRpart[,2])
aucRpart <- auc(rocRpart)

# Graficar la curva ROC
plot(rocRpart, col="red", main=paste("Curva ROC - AUC =", round(aucRpart, 3)))
lines(x=c(0,1), y=c(0,1), col="blue")

predicciones <- predict(pArbolRpart, newdata = testData, type = "class")

# crear tabla de resultados
resultados <- cbind(predicciones, testData$cancelacion)
colnames(resultados) <- c("Predicciones", "Datos Reales")
View(resultados)

```

---

- [Volver a la tabla de contenidos](readme.md)