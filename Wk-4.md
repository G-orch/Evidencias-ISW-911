# Semana 4 Creación y población de tablas del data warehouse

Procedmos a realizar la creación de las tablas del data warehouse para luego migrar la información de utnsa que previamente habíamos cargado con datos provenientes de `Jardineria` mediante la herramienta de migración de datos.

En el poceso de población del data warehouse también se procesó la información para limpiar la iformación y que esta llega de manera correcta al warehouse.

Las siguentes tablas de dimención fueron creadas para el procesamiento de datos:

- DimCliente;
- DimOficina;
- DimEmpleado;
- DimTiempo;
- DimProducto;
- FactPedido;
- DimPedido;

Para crear el cubo ocupamos una herramienta de VS que nos ayuda a la organización de los datos

levantar el instalador de server management
analisis services- tabular (importante), usuario admin de la máquina

Un tabular te permmite trabajar con 2 tablas sin tener una tabla de hechos, es como trabajar en excel

3 formas de mostrar la info:

-Tabular
-Gerencial
-Principales agrupamiento desde el punto de vista gráfico, admin, análisis, más detalle

## Atributos de un KPI SMART


- Específico
- Medible
- Alcanzable
- Relevante
- Basado en un tiempo

```sql
use utndw

CREATE TABLE DimCliente (
  IDCliente int constraint pk_cliente primary key ,	
  CodigoCliente INTEGER,
  NombreCliente VARCHAR(50) NOT NULL,
  NombreContacto VARCHAR(50),
  Ciudad VARCHAR(50) NOT NULL,
  Region VARCHAR(50),
  Pais VARCHAR(50),
  RepresentanteVentas varchar(80),
  LimiteCredito NUMERIC(15,2),
  Categoria varchar(20)
);


CREATE TABLE DimOficina (
  IDOficina  int constraint pk_oficina primary key ,
  CodigoOficina VARCHAR(10) ,
  Ciudad VARCHAR(30) NOT NULL,
  Pais VARCHAR(50) NOT NULL,
  Region VARCHAR(50),
  Jefatura varchar(80)
);

CREATE TABLE DimEmpleado (	
  IDEmpleado  int constraint pk_empleado primary key ,
  CodigoEmpleado INTEGER ,
  NombreCompleto VARCHAR(80) NOT NULL,
  Oficina VARCHAR(50) ,
  Jefe varchar(80),
  Puesto VARCHAR(50)
);


CREATE TABLE DimTiempo (
  Fecha DATE PRIMARY KEY,
  Anio INTEGER NOT NULL,
  Mes INTEGER NOT NULL,
  Dia INTEGER NOT NULL,
  DiaSemana INTEGER NOT NULL,
  NombreMes VARCHAR(20) NOT NULL,
  Trimestre INTEGER NOT NULL,
  Semestre INTEGER NOT NULL
);

-- Parámetros: Fecha Inicial y Fecha Final
DECLARE @FechaInicial DATE = '2021-01-01';
DECLARE @FechaFinal DATE = '2023-12-31';

-- Variable de control para el ciclo
DECLARE @FechaActual DATE = @FechaInicial;
-- Ciclo WHILE para insertar datos en la dimensión de tiempo
WHILE @FechaActual <= @FechaFinal
BEGIN
    INSERT INTO DimTiempo (Fecha, Anio, Mes, Dia, DiaSemana, NombreMes, Trimestre, Semestre)
    VALUES (
        @FechaActual,
        YEAR(@FechaActual),
        MONTH(@FechaActual),
        DAY(@FechaActual),
        DATEPART(WEEKDAY, @FechaActual),
        DATENAME(MONTH, @FechaActual),
        DATEPART(QUARTER, @FechaActual),
        CASE WHEN MONTH(@FechaActual) <= 6 THEN 1 ELSE 2 END
    );

    SET @FechaActual = DATEADD(DAY, 1, @FechaActual);
END;

select *  from DimTiempo

CREATE TABLE DimProducto (
  IDProducto  int constraint pk_prodcuto primary key ,
  CodigoProducto VARCHAR(15),
  Nombre VARCHAR(70) NOT NULL,
  Gama VARCHAR(50) ,
  Proveedor VARCHAR(50),
  PrecioVenta NUMERIC(15,2) NOT NULL,
  PrecioProveedor NUMERIC(15,2)
);


CREATE TABLE FactPedido (
IDCliente int,
IDEmpleado int,
IDProducto int,
IDOficina int,
Fecha date,
Unidades int,
Precio decimal(25,5)
);

alter table FactPedido add constraint fk_pedido_cliente foreign key (idcliente)
  references  dimcliente(idcliente)


alter table FactPedido add constraint fk_pedido_empleado foreign key (idempleado)
  references  dimEmpleado(idempleado)


alter table FactPedido add constraint fk_pedido_oficina foreign key (idoficina)
  references  dimOficina(idOficina)


alter table FactPedido add constraint fk_pedido_producto foreign key (idproducto)
  references  dimproducto(idproducto)


alter table FactPedido add constraint fk_pedido_tiempo foreign key (fecha)
  references  dimtiempo(fecha)



USE utndw

--          Tablas de  inicio de clase

alter table DimProducto add Categoria varchar(20),Estado varchar(20)
alter table FactPedido add IDPedido int
alter table FactPedido add constraint fk_pedido_pedido foreign key(idpedido) 
references dimpedido(idpedido)

-- se crea otra dimension ------------------------------------------------------

create table DimPedido
(IDPedido int identity constraint pk_pedido primary key,
CodigoPedido int,
FechaPedido date,
CondicionEntrega varchar(50),
Estado varchar(30),
Cliente varchar(50))


-- Inicio de clase:-------------------------------------------------------

-- vemos las tablas:
select 'select * from '+ name + ';' from sys.tables

select * from DimCliente;
select * from DimOficina;
select * from DimEmpleado;
select * from DimTiempo;
select * from DimProducto;
select * from FactPedido;
select * from DimPedido;

select * from sys.tables


--------------Inserción de datos cliente-----------------------------


begin
insert into utndw.dbo.DimCliente (
CodigoCliente,
NombreCliente,
NombreContacto,
Ciudad,
Region,
Pais,
RepresentanteVentas,
LimiteCredito,
Categoria)
SELECT upper(c.codigo_cliente)                             CodigoCliente,
       upper(c.nombre_cliente)                             NombreCliente,
       upper(c.nombre_contacto)                            NombreContacto,
       upper(c.ciudad)                                     Ciudad,
       upper(Isnull(c.region, c.ciudad))                   Region,
       upper(c.pais)                                       Pais,
       upper(Isnull(concat(e.nombre ,' ',e.apellido1,' ' ,e.apellido2), 'Alberto Soria Carrasco')) RepresentanteVentas,
       c.limite_credito                             LimiteCredito,
       Isnull(cc.clase, 'PROSPECTO')                Categoria
FROM   utnsa.JARDINERIA.cliente c
       LEFT JOIN utnsa.JARDINERIA.empleado e
              ON c.codigo_empleado_rep_ventas = e.codigo_empleado
       LEFT JOIN (SELECT codigo_cliente,
                         total,
                         CASE
                           WHEN clase = 1 THEN 'SOBRESALIENTE'
                           WHEN clase = 2 THEN 'BUENO'
                           WHEN clase = 3 THEN 'REGULAR'
                         END clase
                  FROM   (SELECT p.codigo_cliente,
                                 Sum(dp.cantidad * dp.precio_unidad)
                                 total,
                                 Ntile(3)
                                   OVER(
                                     ORDER BY Sum(dp.cantidad*dp.precio_unidad)
                                   DESC)
                                 clase
                          FROM   utnsa.JARDINERIA.pedido p
                                 INNER JOIN utnsa.JARDINERIA.detalle_pedido dp
                                         ON p.codigo_pedido = dp.codigo_pedido
                          WHERE  Upper(estado) = 'ENTREGADO'
                          GROUP  BY p.codigo_cliente) a) cc
              ON c.codigo_cliente = cc.codigo_cliente 
end ;

--Modificamos las tablas en click -> sobre la tabla/desing/identity specification= yes


----Insert de oficina------------------------------
select * from DimOficina;

insert into DimOficina(CodigoOficina,Ciudad,Pais,Region,Jefatura)
select 
o.codigo_oficina CodigoOficina,
upper(o.Ciudad) ciudad,
upper(o.Pais) pais,
upper(o.Region) region,
upper(Isnull(concat(e.nombre ,' ',e.apellido1,' ' ,e.apellido2), 'Alberto Soria Carrasco'))  Jefatura
from utnsa.JARDINERIA.oficina o
left join utnsa.JARDINERIA.empleado e
on o.codigo_oficina=e.codigo_oficina
and e.puesto = 'Director Oficina'


-------------------Insert de empleado-------------------------

select * from DimEmpleado;
-- delete from DimEmpleado;
select * from utnsa.JARDINERIA.empleado

insert into DimEmpleado (CodigoEmpleado,NombreCompleto,Oficina,Jefe,Puesto)
select 
e.codigo_empleado codigoempleado,
upper(concat(e.nombre ,' ',e.apellido1,' ' ,e.apellido2)) nombrecompleto,
o.codigo_oficina +' -> ' +upper(o.ciudad) oficina,
case when len(j.nombre)>2 then
trim(upper(concat(trim(j.nombre) ,' ',trim(j.apellido1),' ' ,trim(j.apellido2))))
else 'COMITE GERENCIAL'end  jefe,
upper(e.puesto) puesto
from utnsa.JARDINERIA.empleado e
left join utnsa.JARDINERIA.empleado j
on e.codigo_jefe=j.codigo_empleado
left join utnsa.JARDINERIA.oficina o
on e.codigo_oficina =o.codigo_oficina


-------------Insert de producto --------------------

select * from DimProducto;
select * from  utnsa.JARDINERIA.producto

insert into DimProducto (CodigoProducto,nombre,gama,Proveedor,PrecioVenta,PrecioProveedor,categoria,estado)
select 
pr.codigo_producto,
upper(nombre) nombre,
upper(gama) gama,
upper(proveedor) proveedor,
precio_venta,
precio_proveedor,
concat('Grupo ',isnull(grupo,0)) categoria,
iif(cantidad_en_stock>0 ,'DISPONIBLE','AGOTADO') estado
from utnsa.JARDINERIA.producto pr
left join 
(
SELECT dp.codigo_producto,
            Sum(dp.cantidad * dp.precio_unidad)
            total,
            Ntile(10)
            OVER(
                ORDER BY Sum(dp.cantidad*dp.precio_unidad)
            DESC)
            grupo
    FROM   utnsa.JARDINERIA.pedido p
            INNER JOIN utnsa.JARDINERIA.detalle_pedido dp
                    ON p.codigo_pedido = dp.codigo_pedido
    WHERE  Upper(estado) = 'ENTREGADO'
    GROUP  BY dp.codigo_producto) vp
	on pr.codigo_producto=vp.codigo_producto


	---------------------Insert de pedido-----------------

select * from DimPedido;

insert into DimPedido
(CodigoPedido ,
FechaPedido ,
CondicionEntrega ,
Estado ,
Cliente) 
select 
CODIGO_PEDIDO,
FECHA_PEDIDO,
iif (p.estado='PENDIENTE' , 'PENDIENTE DE ENTREGA',
case when FECHA_ENTREGA>FECHA_ESPERADA then
'ENTREGADO CON RETRASO'
else 
'ENTREGADO A TIEMPO' end),
upper(p.estado),
upper(c.NOMBRE_CLIENTE)
from utnsa.JARDINERIA.pedido p
left join utnsa.JARDINERIA.CLIENTE c
on p.CODIGO_CLIENTE=c.CODIGO_CLIENTE


-----------------Insert de   --------------

select * from FactPedido

--truncate table FactPedido

insert into FactPedido(
IDCliente,
IDEmpleado,
IDProducto,
IDOficina,
IDPedido,
Fecha,
Unidades,
Precio
)
select 
c.IDCliente,
e.IDEmpleado,
pr.IDProducto,
o.IDOficina,
p.IDPedido,
t.Fecha,
cantidad Unidades,
Precio_Unidad Precio

from utnsa.JARDINERIA.detalle_pedido dp
inner join utndw.dbo.dimpedido p
on dp.codigo_pedido=p.CODIGOPEDIDO
inner join utndw.dbo.DimTiempo t
on p.FechaPedido=t.Fecha
inner join DimCliente c
on c.NombreCliente=p.Cliente
left join DimEmpleado e
on e.NombreCompleto=c.RepresentanteVentas
left join DimProducto pr
on dp.codigo_producto=pr.CodigoProducto
left join DimOficina o
on rtrim(SUBSTRING(e.Oficina, 1, CHARINDEX('->', e.Oficina) - 1))=o.CodigoOficina


select * from FactPedido

-----------------Corregido Migue------------------

SET IDENTITY_INSERT utndw.dbo.FactPedido ON
insert into FactPedido(
IDCliente,
IDEmpleado,
IDProducto,
IDOficina,
IDPedido,
Fecha,
Unidades,
Precio
)
select 
c.IDCliente,
e.IDEmpleado,
pr.IDProducto,
o.IDOficina,
p.IDPedido,
t.Fecha,
cantidad Unidades,
Precio_Unidad Precio

from utnsa.JARDINERIA.detalle_pedido dp
inner join utndw.dbo.dimpedido p
on dp.codigo_pedido=p.CODIGOPEDIDO
inner join utndw.dbo.DimTiempo t
on p.FechaPedido=t.Fecha
inner join DimCliente c
on c.NombreCliente=p.Cliente
left join DimEmpleado e
on e.NombreCompleto=c.RepresentanteVentas
left join DimProducto pr
on dp.codigo_producto=pr.CodigoProducto
left join DimOficina o
on rtrim(SUBSTRING(e.Oficina, 1, CHARINDEX('->', e.Oficina) - 1))=o.CodigoOficina
SET IDENTITY_INSERT utndw.dbo.FactPedido OFF

--------------------listo llenado de datos en DW---------------------

select * from DimCliente;
select * from DimOficina;
select * from DimEmpleado;
select * from DimTiempo;
select * from DimProducto;
select * from FactPedido;
select * from DimPedido;

```

---

- [Volver a la tabla de contenidos](readme.md)