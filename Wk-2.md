# Semana 2

## Análisis de nuevas funciones: 

- LAG-LEAD: Se utilizan para acceder a un valor de una fila anterior o siguiente en un conjunto de resultados, basado en un orden especificado.
- Ntile: Divide el conjunto de resultados en "n" partes iguales y asigna un número a cada fila según la parte a la que pertenece.
- Partition by: Divide el conjunto de resultados en particiones según los valores de una o más columnas
- Listagg: Concatena los valores de una columna en una sola cadena, con un separador opcional.
- Pivot: Transforma las filas en columnas, basándose en los valores de una columna específica.
- Unpivot: Es lo opuesto a Pivot
- Rollup: Genera subtotales y totales en un conjunto de resultados al seguir un orden de columnas
- Cube: Similar a ROLLUP, pero genera todas las combinaciones posibles de subtotales y totales.
- Coalesce: Retorna el primer valor no nulo en una lista de expresiones.
- Nvl: Similar a COALESCE, devuelve el segundo valor si el primero es nulo.
- Decode: Realiza evaluaciones condicionales en una sola línea.
- Case: Realiza evaluaciones condicionales más flexibles y complejas; sirve como decode pero más potente

### Datawarehouse
- repositorio centralizado de información
- historico
- no volatil 
- orientado a consultas, 

Estructuras:

1. Tabla de dimensiones: elementos característicos de la entidad (atributos) valores numéricos solo descriptivos. Ej: clientes/productos. Podemos almacenar info de 3 tipos: 

   a- info que sobreescribe
   b- se genera un nuevo registro con los datos que cambiaron
   c- nueva versión actual y una anterior de los datos.
   
2. Tabla de hechos: trasnsacciones, almacena las mediciones o métricas de negocios, contiene hechos y claves externas a las tablas de dimensiones.
   
3. Tabla agregada: tabla de resumen, contiene datos que se resumen hasta cierto nivel de detalle.

### Diagrama de estrella: 
Desnormalización: oreintado a consultas, diseño de BD agregando datos redundantes o agrupando

### Esquema de snow flake: 
tipo de esquema en estrella en el que las tablas de dimenciones están normalizadas parcial o totalmente

### Esquema de constelación:
 modelo de estrella extendida o galaxia, variante del modelo de esquema dimensional utilizado en la construcción de data warehouses

![Imagen](./images/modelo-de-datos-power-bi-copo-de-nieve-estrella-1024x473.jpg)

---

- [Volver a la tabla de contenidos](readme.md)