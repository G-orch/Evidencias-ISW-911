# Semana 3

## Técnicas para transferencia de datos 

Se debe de instalar un sql server versión de desarrollo `developer`
Se deben de traer cargadas las tablas de jardinería

crear utnsa como stagyn 
crear utndw cmo datawarehouse


con visual studio instalado, seguir las indicaciones para instalar la interfaz que ayudará a la transferencia de datos:

[Data Tools](https://learn.microsoft.com/es-es/sql/ssdt/download-sql-server-data-tools-ssdt?view=sql-server-ver16#ssdt-for-visual-studio-2022)

analysis services
servicios de integracion

* nuevo proyecto: `integration services project`

## Forma 1 de transferencia de datos: Data tools

- pros: interfaz grafica, multiples componentes y conectores; administración e infraestructura , se puede desplegar en un job, fácil de entender el flujo.

- contras: curva de aprendizaje puede ser muy amplia por los componentes que ejecutan una acción en específico

Configuraciones iniciales:

origen de datos: administrador de conecciones por OLEDB, se busca driver: oracle date ax client
server or file name: local
username: northwind  /  local.northwind.conmgr

conexion con otra: ej:  freddy.utn.conmrg

- data flow task: arrastrar al centro a flujo de control

- origen de dato arrastrar OLDB Source

## Forma 2 de transferencia de datos:

Código/ Python bullk collect 

pros: muy potente para el manejo de estructuras de datos, se  diseña una única vez el script que es dinámico,
contras: seguridad ya que es un archivo de texto.

Script de ejemplo:

```python
import pyodbc
from sqlalchemy import create_engine
import cx_Oracle

cx_Oracle.init_oracle_client(lib_dir=r"C:/oracle/instantclient_21_13")

def ora_conndb():
    server = "localhost/xe"
    username = 'jardineria'
    pwd = 'Admin12345'

    connection = cx_Oracle.connect(
        user=username, password=pwd, dsn=server
    )
    return connection

def mssql_conndb(method=1):
    server = 'FREDDY'
    database = 'utnsa'
    username = 'sa'
    password = 'Admin12345'

    if method == 1:
        conexion_str = (
            'DRIVER={SQL Server};SERVER=' + server + ';DATABASE=' + database +
            ';UID=' + username + ';PWD=' + password
        )
        conexion = pyodbc.connect(conexion_str)

    if method == 2:
        DATABASE_URL = (
            f'mssql+pyodbc://{username}:{password}@{server}/{database}'
            '?driver=ODBC+Driver+17+for+SQL+Server'
        )
        conexion = create_engine(DATABASE_URL)

    return conexion

source_conn = ora_conndb()
target_conn = mssql_conndb(1)


def get_user_tables():
    connection = ora_conndb()
    cursor = connection.cursor()
    try:
        cursor.execute("SELECT table_name FROM user_tables where table_name in ('CLIENTE','PEDIDO')")
        tables = [row[0] for row in cursor.fetchall()]
        return tables
    except Exception as e:
        print(f"Error al obtener las tablas de usuario: {str(e)}")
        return None
    finally:
        cursor.close()
        connection.close()

def get_oracle_data(owner, table_name):
    cursor = source_conn.cursor()
    try:
        sql = f"SELECT * FROM {owner}.{table_name}"
        cursor.execute(sql)
        data = cursor.fetchall()
        return data
    except Exception as e:
        print(f"Error al obtener datos de Oracle: {str(e)}")
        return None
    finally:
        cursor.close()

def count_columns(table):
    cursor = source_conn.cursor()
    select_query = f"SELECT COUNT(*) FROM user_tab_columns WHERE table_name = '{table.upper()}'"
    cursor.execute(select_query)
    result = cursor.fetchone()[0]
    cursor.close()
    return result

def create_sql_insert(owner, table, numfields):
    # antes de insertar deben estar la tabla creada en SQL server 
    # SELECT DBMS_METADATA.GET_DDL('TABLE', 'CLIENTE') FROM DUAL;
    sql = f"INSERT INTO {owner}.{table} VALUES ({', '.join(['?' for _ in range(numfields)])})"
    return sql


def add_entity(owner,tablename):
    numfields = count_columns(tablename)
    data = get_oracle_data(owner, tablename)
    sql = create_sql_insert("dbo", tablename, numfields)

    if data:
        target_cursor = target_conn.cursor()
        target_cursor.executemany(sql, data)
        target_conn.commit()
        target_cursor.close()

def delete_data(table, operation, connection):
    cursor = connection.cursor()
    try:
        if operation.upper() == 'DELETE':
            delete_query = f"DELETE FROM {table}"
            cursor.execute(delete_query)
        elif operation.upper() == 'TRUNCATE':
            truncate_query = f"TRUNCATE TABLE {table}"
            cursor.execute(truncate_query)
        else:
            raise ValueError("Operación no válida. Use 'DELETE' o 'TRUNCATE'.")
        
        connection.commit()
    except Exception as e:
        print(f"Error al realizar la operación: {str(e)}")
    finally:
        cursor.close()

if __name__ == "__main__":
    owner='jardineria'
    user_tables = get_user_tables()
    if user_tables:
        print("Transfiriendo informacion:")
        for table in user_tables:
            delete_data(table, 'DELETE', target_conn)
            add_entity(owner,table)
            print ("Transferencia de "+str(table) +" Completa")
    else:
        print("No se pudieron obtener las tablas de usuario.")
    source_conn.close()
    target_conn.close()
    print("Inserción de datos SQL Server completada.")
```

 
 ## Forma 3 de transferencia de datos:

Liga de base de datos

```sql

select * from empleados

select * into empleados  from (
select * from openquery(oradb,'select * from empleado'))da

insert into empleados 
select * from openquery(oradb,'select * from empleado')da
```

## Scripts para creación de tablas de dimensión y hechos

```sql
use utndw

CREATE TABLE DimCliente (
  IDCliente int constraint pk_cliente primary key ,	
  CodigoCliente INTEGER,
  NombreCliente VARCHAR(50) NOT NULL,
  NombreContacto VARCHAR(50),
  Ciudad VARCHAR(50) NOT NULL,
  Region VARCHAR(50),
  Pais VARCHAR(50),
  RepresentanteVentas varchar(80),
  LimiteCredito NUMERIC(15,2),
  Categoria varchar(20)
);


CREATE TABLE DimOficina (
  IDOficina  int constraint pk_oficina primary key ,
  CodigoOficina VARCHAR(10) ,
  Ciudad VARCHAR(30) NOT NULL,
  Pais VARCHAR(50) NOT NULL,
  Region VARCHAR(50),
  Jefatura varchar(80)
);

CREATE TABLE DimEmpleado (	
  IDEmpleado  int constraint pk_empleado primary key ,
  CodigoEmpleado INTEGER ,
  NombreCompleto VARCHAR(80) NOT NULL,
  Oficina VARCHAR(50) ,
  Jefe varchar(80),
  Puesto VARCHAR(50)
);


CREATE TABLE DimTiempo (
  Fecha DATE PRIMARY KEY,
  Anio INTEGER NOT NULL,
  Mes INTEGER NOT NULL,
  Dia INTEGER NOT NULL,
  DiaSemana INTEGER NOT NULL,
  NombreMes VARCHAR(20) NOT NULL,
  Trimestre INTEGER NOT NULL,
  Semestre INTEGER NOT NULL
);

-- Parámetros: Fecha Inicial y Fecha Final
DECLARE @FechaInicial DATE = '2021-01-01';
DECLARE @FechaFinal DATE = '2023-12-31';

-- Variable de control para el ciclo
DECLARE @FechaActual DATE = @FechaInicial;
-- Ciclo WHILE para insertar datos en la dimensión de tiempo
WHILE @FechaActual <= @FechaFinal
BEGIN
    INSERT INTO DimTiempo (Fecha, Anio, Mes, Dia, DiaSemana, NombreMes, Trimestre, Semestre)
    VALUES (
        @FechaActual,
        YEAR(@FechaActual),
        MONTH(@FechaActual),
        DAY(@FechaActual),
        DATEPART(WEEKDAY, @FechaActual),
        DATENAME(MONTH, @FechaActual),
        DATEPART(QUARTER, @FechaActual),
        CASE WHEN MONTH(@FechaActual) <= 6 THEN 1 ELSE 2 END
    );

    SET @FechaActual = DATEADD(DAY, 1, @FechaActual);
END;

select *  from DimTiempo

CREATE TABLE DimProducto (
  IDProducto  int constraint pk_prodcuto primary key ,
  CodigoProducto VARCHAR(15),
  Nombre VARCHAR(70) NOT NULL,
  Gama VARCHAR(50) ,
  Proveedor VARCHAR(50),
  PrecioVenta NUMERIC(15,2) NOT NULL,
  PrecioProveedor NUMERIC(15,2)
);


CREATE TABLE FactPedido (
IDCliente int,
IDEmpleado int,
IDProducto int,
IDOficina int,
Fecha date,
Unidades int,
Precio decimal(25,5)
);

alter table FactPedido add constraint fk_pedido_cliente foreign key (idcliente)
  references  dimcliente(idcliente)


alter table FactPedido add constraint fk_pedido_empleado foreign key (idempleado)
  references  dimEmpleado(idempleado)


alter table FactPedido add constraint fk_pedido_oficina foreign key (idoficina)
  references  dimOficina(idOficina)


alter table FactPedido add constraint fk_pedido_producto foreign key (idproducto)
  references  dimproducto(idproducto)


alter table FactPedido add constraint fk_pedido_tiempo foreign key (fecha)
  references  dimtiempo(fecha)


USE utndw

Tablas de  inicio de clase Wk-4

alter table DimProducto add Categoria varchar(20),Estado varchar(20)
alter table FactPedido add IDPedido int
alter table FactPedido add constraint fk_pedido_pedido foreign key(idpedido) 
references dimpedido(idpedido)

-- se crea otra dimension 

create table DimPedido
(IDPedido int identity constraint pk_pedido primary key,
CodigoPedido int,
FechaPedido date,
CondicionEntrega varchar(50),
Estado varchar(30),
Cliente varchar(50))


```


---

- [Volver a la tabla de contenidos](readme.md)