# Semana 6 Introducción a R Studio

Se brinda un resumen de la clase anterior




![Imagen](./images/Wk-5.1.png)

Se muestran los fundamentos de R

```R
install.packages(c("dply", "ggplot"), dependencies = T)
  
library(hexbin)
library(ggplot2)
library(dplyr)
library(dplyr)
library(ggplot2)
library(visdat)
library(naniar)
library(ggplot2)
library(stringr)
library(visdat)

getwd()
setwd()

datos<-12
rm(datos)
datos<-read.csv(file = "Files/Pasajeros.csv", header = T,sep = ",", encoding ="UTF-8")

x = l
class(x)
head(datos)
class(datos)
names(datos)
glimpse(datos)
str(datos)
dim(datos)
colnames(datos)
```

![Imagen](./images/Captura%20de%20pantalla%202024-03-03%20234424.png)

---

- [Volver a la tabla de contenidos](readme.md)